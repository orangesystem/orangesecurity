﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeSecurity
{
    public class OSCryptoServiceProvider
    {
        public byte[] Key { get; }
        Hash HashCode { get { return _hashcode; } }

        private Hash _hashcode;
        /// <summary>
        /// Gera um codigo Hash com base na entrada de dados
        /// </summary>
        /// <param name="input">Dados a serem codificados</param>
        /// <returns>Algorítmo para a criptografia</returns>
        public byte[] ComputeHash(byte[] key, byte[] input)
        {
            _hashcode = new Hash(key, input);
            
            return HashCode.HashCode;
        }

        public IOSTransformationInterface GetHash()
        {
            return HashCode;
        }
    }

    internal partial class Hash : IOSTransformationInterface
    { 
        byte[] Assinatura { get { return _assinatura; } }
        byte Verificador { get { return _verificador; } }
        /// <summary>
        /// Sentido da leitura
        /// </summary>
        byte[] Leitura { get { return _leitura; } }
        /// <summary>
        /// Indica que a partir desse byte não há compactação
        /// </summary>
        byte[] LeituraLivre { get { return _leituraLivre; } }
        byte Abertura { get { return _abertura; } }
        byte Fechamento { get { return _fechamento; } }
        public byte[] HashCode {
            get
            {
                List<byte> codigo = new List<byte>();
                codigo.Add(Assinatura[0]);
                codigo.Add(Assinatura[1]);
                codigo.Add(Verificador);
                codigo.Add(Leitura[0]);
                codigo.Add(Leitura[1]);
                codigo.Add(LeituraLivre[0]);
                codigo.Add(LeituraLivre[1]);
                codigo.Add(Abertura);
                codigo.Add(Fechamento);

                return codigo.ToArray();
            }
        }
        public EncryptionSecurityLevel SecurityLevel { get { return _securityLevel; } set { _securityLevel = value; } }

        Dictionary<byte[], byte[]> Compactacao = new Dictionary<byte[], byte[]>();
        Dictionary<byte, byte> Transposicao = new Dictionary<byte, byte>();
        private List<byte> UsedBytes = new List<byte>();

        private byte[] _assinatura = new byte[2];
        private byte[] _input;
        private byte _verificador;
        private byte[] _leitura = new byte[2];
        private byte[] _leituraLivre = new byte[2];
        private byte _abertura;
        private byte _fechamento;
        private EncryptionSecurityLevel _securityLevel = EncryptionSecurityLevel.MaxSecurity;

        public Hash(byte[] key, byte[] input)
        {
            GenerateSignature(key);
            GenerateVerificator(key);
            GenerateOpenerAndCloser(key);
            GenerateReaderDirection(key);
            GenerateFreeReadComand(key);
            _input = input;
            FillTranspor(key, input);
        }

        public Hash(byte[] key, byte[] input, EncryptionSecurityLevel nivelDeSegurança)
        {
            GenerateSignature(key);
            GenerateVerificator(key);
            GenerateOpenerAndCloser(key);
            GenerateReaderDirection(key);
            GenerateFreeReadComand(key);
            _input = input;
            _securityLevel = nivelDeSegurança;
            FillTranspor(key, input);
        }

        /// <summary>
        /// Preenche o dicionario de transposição e verifica se há necessidade de agrupamento de bytes (compactação)
        /// </summary>
        /// <param name="Key">Chave usada para a geração dos valores</param>
        /// <param name="input">Bytes a serem transpostos</param>
        private void FillTranspor(byte[] Key, byte[] input)
        {
            //Escreve os bytes no dicionário de transposição
            for (int i = 0; i < 256; i++)
            {
                Random r = null;
                byte rValue;
                if (i < Key.Length)
                {
                    r = new Random(Key[i]);
                    rValue = (byte)r.Next(256);
                }

                else
                {
                    r = new Random(i);
                    rValue = (byte)r.Next(256);
                }

                while (Transposicao.ContainsValue(rValue))
                {
                    rValue++;

                    if ((int)rValue >= 256)
                    {
                        rValue = 0;
                    }
                }

                Transposicao.Add((byte)i, rValue);
            }


            //Verifica se há necessidade de criar compactação de dados da seguinte maneira:
            //Insere o byte no dicionário de transposição e verifica sua saída.
            //O byte que saiu é verificado com os bytes reservados, caso haja bytes iguais então gera o comando de
            //compactação de dados, informando como parâmetro, os bytes necessários
            List<byte> CompactNeedBytes = new List<byte>();
            foreach (byte currentByte in input)
            {
                if (UsedBytes.Contains(Transposicao[currentByte]))
                {
                    if (!CompactNeedBytes.Contains(currentByte))
                    { CompactNeedBytes.Add(currentByte); }
                }
            }

            CompactBytes(CompactNeedBytes.ToArray(), input);
        }

        /// <summary>
        /// Gera os bytes de assinatura com base na chave
        /// </summary>
        /// <param name="key">Chave usada na criptografia</param>
        private void GenerateSignature(byte[] key)
        {
            if (key.Length < 8)
            { throw new InvalidKeyException("Uma chave válida precisa conter pelo menos 8 bytes!"); }

            Random r = null;
            r = new Random(key[0] * key[2] - key[4]);
            _assinatura[0] = (byte)r.Next(256);

            r = new Random(key[2] - key[3] + key[1]);
            _assinatura[1] = (byte)r.Next(256);

            UsedBytes = _assinatura.ToList();
        }

        /// <summary>
        /// Gera o verificador de assinatura com base na chave
        /// </summary>
        /// <param name="key">Chave usada na criptografia</param>
        private void GenerateVerificator(byte[] key)
        {
            //O byte de verificação é gerado a partir do segundo byte[1] da chave

            if (key.Length < 8)
            { throw new InvalidKeyException("Uma chave válida precisa conter pelo menos 8 bytes!"); }

            Random r = new Random(key[0] + ((key[1] * key[2]) * key[3]) + key[4] - (key[5] * key[6]) - key[7]);
            _verificador = (byte)r.Next(256);

            while (UsedBytes.Contains(_verificador))
            {
                _verificador += 2;

                if ((int)_verificador >= 256)
                { _verificador = 0; }
            }

            UsedBytes.Add(_verificador);
        }

        /// <summary>
        /// Gera os bytes de abertura e de fechamento de parametro com base na chave
        /// </summary>
        /// <param name="key">Chave usada na criptografia</param>
        private void GenerateOpenerAndCloser(byte[] key)
        {
            //Os bytes de abertura e fechamento de parâmetro são gerados a partir do quinto[4] e sexto[5] byte da chave
            if (key.Length < 8)
            { throw new InvalidKeyException("Uma chave válida precisa conter pelo menos 8 bytes!"); }

            Random r = new Random(key[4] * key[2]);
            _abertura = (byte)r.Next(256);
            while (UsedBytes.Contains(_abertura))
            {
                _abertura += 3;

                if ((int)_abertura >= 256)
                { _abertura = 0; }
            }
            UsedBytes.Add(_abertura);

            r = new Random(key[5] - key[4] + (key[5] * key[6]));
            _fechamento = (byte)r.Next(256);
            while (UsedBytes.Contains(_fechamento))
            {
                _fechamento += 3;

                if ((int)_fechamento >= 256)
                { _fechamento = 0; }
            }
            UsedBytes.Add(_fechamento);
        }

        /// <summary>
        /// Gera os bytes responsáveis pelo sentido da leitura
        /// </summary>
        /// <param name="key">Chave usada na criptografia</param>
        private void GenerateReaderDirection(byte[] key)
        {
            if (key.Length < 8)
            { throw new InvalidKeyException("Uma chave válida precisa conter pelo menos 8 bytes!"); }

            Random r = new Random(key[4] * key[3] - key[0] + key[7]);
            _leitura[0] = (byte)r.Next(256);

            while (UsedBytes.Contains(_leitura[0]))
            {
                _leitura[0] += 1;

                if ((int)_leitura[0] >= 256)
                {
                    _leitura[0] = 0;
                }
            }
            UsedBytes.Add(_leitura[0]);

            r = new Random(key[7] - key[6] + (key[3] * (key[7]* key[1])));
            _leitura[1] = (byte)r.Next(256);

            while (UsedBytes.Contains(_leitura[1]))
            {
                _leitura[1] += 1;

                if ((int)_leitura[1] >= 256)
                {
                    _leitura[1] = 0;
                }
            }
            UsedBytes.Add(_leitura[1]);
        }

        /// <summary>
        /// Gera os bytes responsáveis pela indicação de leitura livre
        /// </summary>
        /// <param name="key">Chave usada na criptografia</param>
        private void GenerateFreeReadComand(byte[] key)
        {
            if (key.Length < 8)
            { throw new InvalidKeyException("Uma chave válida precisa conter pelo menos 8 bytes!"); }

            Random r = new Random(key[1]);
            _leituraLivre[0] = (byte)r.Next(256);

            while (UsedBytes.Contains(_leituraLivre[0]))
            {
                _leituraLivre[0] += 1;

                if ((int)_leituraLivre[0] >= 256)
                { _leituraLivre[0] = 0; }
            }

            _leituraLivre[1] = _leituraLivre[0];
            UsedBytes.Add(_leituraLivre[0]);
        }

        /// <summary>
        /// Prepara os bytes para serem compactados de forma inteligente e os compacta
        /// </summary>
        /// <param name="preferencialBytes">Bytes preferenciais</param>
        /// <param name="input">entrada de dados a ser criptografada</param>
        private void CompactBytes(byte[] preferencialBytes, byte[] input)
        {
            List<byte[]> BestGroups = CheckForPreferencialCompactation(preferencialBytes, input);
            List<byte[]> BestNormalGroups = CheckForNormalCompactation(input);

            List<byte[]> concreteBestGroup = SelectConcretBestGroup(BestNormalGroups, BestGroups);
            foreach (byte[] currentGroup in concreteBestGroup)
            { GenerateCompactByte(currentGroup, input); }
        }

        /// <summary>
        /// Procura dentro da input sequencias repetidas de bytes que podem ser compactadas
        /// </summary>
        /// <param name="input">Input a ser analisada</param>
        /// <returns>Grupo compactado de bytes ja otimizado com os melhores</returns>
        private List<byte[]> CheckForNormalCompactation(byte[] input)
        {
            List<List<byte[]>> foundNormalGroupedBytes = new List<List<byte[]>>();

            int currentList = 0;
            for (int arraySize = 2; arraySize < input.Length; arraySize++)
            {
                for (int initialByte = 0; initialByte < input.Length; initialByte++)
                {
                    int currentIndex = 0;
                    List<byte> chosenBytes = new List<byte>();
                    try
                    {
                        for (int index = 0; index < arraySize; index++)
                        { chosenBytes.Add(input[index + initialByte]); currentIndex = (index + initialByte); }
                    }
                    catch (IndexOutOfRangeException) { break; }

                    if (CountGroups(chosenBytes.ToArray(), input, currentIndex) > 2)
                    {
                        try
                        { foundNormalGroupedBytes[currentList].Add(chosenBytes.ToArray()); }

                        catch (ArgumentOutOfRangeException)
                        {
                            foundNormalGroupedBytes.Add(new List<byte[]>());
                            foundNormalGroupedBytes[currentList].Add(chosenBytes.ToArray());
                        }
                    }
                }
                currentList++;
            }

            List<byte[]> BestNormalGroups = SelectBestGroupsToCompact(foundNormalGroupedBytes.ToArray(), input);
            return BestNormalGroups;
        }

        /// <summary>
        /// Procura dentro da input sequencia repetidas de bytes preferenciais para serem compactados. Caso não
        /// encontre sequencias, retorna apenas os bytes preferenciais.
        /// </summary>
        /// <param name="preferencialBytes">Bytes preferenciais previamente encontrados</param>
        /// <param name="input">Entrada de dados a ser analisada</param>
        /// <returns>Sequência otimizada com os melhores grupos.</returns>
        private List<byte[]> CheckForPreferencialCompactation(byte[] preferencialBytes, byte[] input )
        {
            //Primeiro analisa a existencia dos bytes preferenciais
            //Verifica se há grupos de bytes preferenciais.
            //Se houver, gera a chave de compactação
            byte[] groupedBytes;
            List<byte[]>[] foundGroupedBytes = new List<byte[]>[preferencialBytes.Length];
            if (preferencialBytes.Length > 0)
            {
                for (int i = 0; i < foundGroupedBytes.Length; i++)
                { foundGroupedBytes[i] = new List<byte[]>(); }

                try
                {
                    for (int arraySize = 0; arraySize < preferencialBytes.Length; arraySize++)
                    {
                        for (int initialByte = 0; initialByte < preferencialBytes.Length; initialByte++)
                        {
                            List<byte> chosenBytes = new List<byte>();
                            for (int index = 0; index < arraySize; index++)
                            { chosenBytes.Add(preferencialBytes[index + initialByte]); }

                            groupedBytes = chosenBytes.ToArray();
                            if (CompareBytes(groupedBytes, input))
                            { foundGroupedBytes[arraySize].Add(groupedBytes); }
                        }
                    }
                }
                catch (IndexOutOfRangeException) { }
            }

            List<byte[]> BestGroups = new List<byte[]>();
            if (preferencialBytes.Length > 0)
            {
                try
                {
                    if (foundGroupedBytes[0].Count > 0)
                    { BestGroups = SelectBestGroupsToCompact(foundGroupedBytes, input); }

                    else
                    {
                        foreach (byte currentByte in preferencialBytes)
                        {
                            byte[] b = new byte[1] { currentByte };
                            foundGroupedBytes[0].Add(b);
                        }

                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    foundGroupedBytes = new List<byte[]>[1] { new List<byte[]>() };
                    foreach (byte currentByte in preferencialBytes)
                    {
                        byte[] b = new byte[1] { currentByte };
                        foundGroupedBytes[0].Add(b);
                    }
                }

                finally
                {
                    BestGroups = SelectBestGroupsToCompact(foundGroupedBytes, input);
                }
            }

            return BestGroups;
        }
        
        /// <summary>
        /// Verifica dentro do input se os bytes fornecidos estão agrupados
        /// </summary>
        /// <param name="groupedBytes">Bytes a serem verificados</param>
        /// <param name="input">Input a ser codificada</param>
        /// <returns>Verdadeiro se houver, em algum local da input, bytes agrupados</returns>
        private bool CompareBytes(byte[] groupedBytes, byte[] input)
        {
            bool equality = false;
            List<byte> entradas = input.ToList();

            if (input.Length >= groupedBytes.Length)
            {
                while (groupedBytes.Length <= entradas.Count)
                {
                    for (int i = 0; i < groupedBytes.Length; i++)
                    {
                        if (groupedBytes[i] == entradas[i])
                        { equality = true; }
                        else { equality = false; break; }
                    }

                    if (equality) { break;
                    }
                    else { if (entradas.Count > 0) entradas.RemoveAt(0); else break; }
                }
            }

            return equality;
        }

        /// <summary>
        /// Verifica dentro da input, a partir de determinada posição, se os bytes fornecidos estão agrupados.
        /// </summary>
        /// <param name="groupedBytes">Bytes a serem verificados</param>
        /// <param name="input">Entrada a ser verificada</param>
        /// <param name="initialIndex">índice com base 0, a ser verificado</param>
        /// <returns>Verdadeiro se houver a ocorrência de pelo menos um grupo igual</returns>
        private bool CompareBytes(byte[] groupedBytes, byte[] input, int initialIndex)
        {
            bool equality = false;
            List<byte> entradas = input.ToList();
            entradas.RemoveRange(0, initialIndex);

            if (input.Length >= groupedBytes.Length)
            {
                while (groupedBytes.Length <= entradas.Count)
                {
                    for (int i = 0; i < groupedBytes.Length; i++)
                    {
                        if (groupedBytes[i] == entradas[i])
                        { equality = true; }
                        else { equality = false; break; }
                    }

                    if (equality)
                    {
                        break;
                    }
                    else { if (entradas.Count > 0) entradas.RemoveAt(0); else break; }
                }
            }

            return equality;
        }

        /// <summary>
        /// Verifica dentro da input, a partir de determinada posição, a quantidade de grupos que correspondem aos dados fornecidos
        /// </summary>
        /// <param name="groupedBytes">Grupo a ser contado</param>
        /// <param name="input">Entrada a ser verificada a quantidade de grupos</param>
        /// <param name="initialIndex">Índice inicial a partir da entrada de dados</param>
        /// <returns>Numero de grupos encontrados</returns>
        private int CountGroups(byte[] groupedBytes, byte[] input, int initialIndex)
        {
            bool equality = false;
            List<byte> entradas = input.ToList();
            entradas.RemoveRange(0, initialIndex);
            int numberOfGroups = 0;

            if (input.Length >= groupedBytes.Length)
            {
                while (groupedBytes.Length <= entradas.Count)
                {
                    for (int i = 0; i < groupedBytes.Length; i++)
                    {
                        if (groupedBytes[i] == entradas[i])
                        { equality = true; }
                        else { equality = false; break; }
                    }

                    if (equality)
                    {
                        numberOfGroups++;
                    }
                    if (entradas.Count > 0) { entradas.RemoveAt(0); } else { break; }
                }
            }

            return numberOfGroups;
        }

        /// <summary>
        /// Seleciona de forma inteligente os melhores grupos de bytes para serem compactados
        /// </summary>
        /// <param name="foundGroupedBytes">Todos os grupos encontrados</param>
        /// <param name="input">Bytes a serem criptografados</param>
        private List<byte[]> SelectBestGroupsToCompact(List<byte[]>[] foundGroupedBytes, byte[] input)
        {
            List<byte> previewInput = input.ToList();
            List<byte[]> BestGroups = new List<byte[]>();

            for (int i = foundGroupedBytes.Length - 1; i >= 0; i--)
            {
                if (foundGroupedBytes[i] != null)
                {
                    for (int j = foundGroupedBytes[i].Count - 1; j >= 0; j--)
                    {
                        if (CompareBytes(foundGroupedBytes[i][j], previewInput.ToArray()))
                        {
                            BestGroups.Add(foundGroupedBytes[i][j]);
                            previewInput = RemoveFromInput(foundGroupedBytes[i][j], previewInput);
                        }
                    }
                }
            }

            return BestGroups;
        }

        /// <summary>
        /// Compara os dois grupos de bytes e seleciona os melhores para serem compactados
        /// </summary>
        /// <param name="BestNormalGroups">Grupos de bytes normais com pre-seleção de melhores bytes a serem compactados</param>
        /// <param name="BestPreferencialGroups">Grupos de bytes preferenciais com pre-seleção de melhores bytes a serem compactados</param>
        /// <returns>Os melhores grupos de bytes para serem compactados</returns>
        private List<byte[]> SelectConcretBestGroup(List<byte[]> BestNormalGroups, List<byte[]> BestPreferencialGroups)
        {
            //Verifica primeiramente, quais grupos preferenciais estão contidos dentro dos grupos normais.
            //Estes serão selecionados.
            //logo após, verifica se ha grupos contendo apenas um byte no grupo de byte preferenciais, estes serão adicionados 
            //ao grupo concreto pois são preferenciais e não há como ignorá-los;
            //Todos os grupos normais são utilizados. Não há grupos normais contendo apenas um byte.
            List<byte[]> normalGroups = BestNormalGroups.ToList();
            List<byte[]> preferencialGroups = BestPreferencialGroups.ToList();
            List<byte[]> concreteGroup = new List<byte[]>();

            List<int> indexToRemove = new List<int>();
            int currentIndex = 0;
            foreach(byte[] currentPreferencialGroup in preferencialGroups)
            {
                foreach(byte[] currentNormalGroup in normalGroups)
                {
                    if(CompareBytes(currentPreferencialGroup, currentNormalGroup))
                    {
                        indexToRemove.Add(currentIndex);
                    }
                }
                currentIndex++;
            }

            List<byte[]> bytesToRemove = new List<byte[]>();
            foreach(int i in indexToRemove)
            { bytesToRemove.Add(preferencialGroups[i]); }

            foreach(byte[] bytesToKill in bytesToRemove)
            {
                preferencialGroups.Remove(bytesToKill);
            }

            foreach(byte[] currentGroup in preferencialGroups)
            { concreteGroup.Add(currentGroup); }

            foreach(byte[] currentGroup in normalGroups)
            { concreteGroup.Add(currentGroup); }

            return concreteGroup;
        }

        /// <summary>
        /// Remove da input, de forma segura, os grupos de bytes selecionados
        /// </summary>
        /// <param name="bytesToRemove">Bytes a serem removidos</param>
        /// <param name="input">Entrada que os dados serão removidos</param>
        /// <returns>Retorna uma nova lista com base na input, sem os grupos de bytes selecionados para a remoção</returns>
        private List<byte> RemoveFromInput(byte[] bytesToRemove, List<byte> input)
        {
            bool canRemove = false;
            int startIndex = 0;
            List<byte> newInput = input;
            while (CompareBytes(bytesToRemove, newInput.ToArray()))
            {
                try {
                    for (int inicialIndex = 0; inicialIndex < input.Count; inicialIndex++)
                    {
                        if (bytesToRemove[0] == input[inicialIndex])
                        {
                            for (int i = 0; i < bytesToRemove.Length; i++)
                            {
                                if (bytesToRemove[i] == input[inicialIndex + i])
                                { canRemove = true; }
                                else { canRemove = false; break; }
                            }
                            if (canRemove) { startIndex = inicialIndex; break; }
                        }
                    }
                    newInput.RemoveRange(startIndex, bytesToRemove.Length);
                }
                catch(IndexOutOfRangeException) { break; }
            }

            return newInput;
        }

        /// <summary>
        /// Altera da input, de forma segura, os grupos de bytes selecionados pelo seu simétrico de compactação
        /// </summary>
        /// <param name="bytesToRemove">Bytes a serem alterados</param>
        /// <param name="input">Entrada que os dados serão removidos</param>
        /// <returns>Retorna uma nova lista com base na input, sem os grupos de bytes selecionados para a remoção</returns>
        private List<byte> ReplaceFromInput(byte[] bytesToReplace, List<byte> input)
        {
            bool canRemove = false;
            List<int> startIndexes = new List<int>();
            List<byte> newInput = new List<byte>(input);
            while (CompareBytes(bytesToReplace, newInput.ToArray()))
            {
                try
                {
                    for (int inicialIndex = 0; inicialIndex < newInput.Count; inicialIndex++)
                    {
                        if (bytesToReplace[0] == newInput[inicialIndex])
                        {
                            for (int i = 0; i < bytesToReplace.Length; i++)
                            {
                                if (bytesToReplace[i] == newInput[inicialIndex + i])
                                { canRemove = true; }
                                else { canRemove = false; break; }
                            }
                            if (canRemove)
                            {
                                newInput.RemoveRange(inicialIndex, bytesToReplace.Length);
                                newInput.InsertRange(inicialIndex, Compactacao[bytesToReplace]);
                                break;
                            }
                        }
                    }
                }
                catch (IndexOutOfRangeException) { break; }
            }

            return newInput;
        }

        /// <summary>
        /// Assina, dentro da entrada de dados, os bytes compactados
        /// </summary>
        /// <param name="replacedBytes">Bytes que foram compactados</param>
        /// <param name="input">Entrada a ser assinada</param>
        /// <returns>Entrada assinada</returns>
        private byte[] SignCompact(byte[] replacedBytes, List<byte> input)
        {
            Random r = new Random(input.Count);
            int index = r.Next(input.Count);
            try {
                if (IsInsideAProperty(index, input.ToArray()))
                { index = GoToNextSection(r.Next(input.Count), input.ToArray()); }
            }
            catch(Exception)
            {
                while(IsInsideAProperty(index, input.ToArray()))
                { index = r.Next(input.Count); }
            }

            List<byte> assinatura = new List<byte>();
            assinatura.Add(Abertura);
            byte[] compact = Compactacao[replacedBytes];

            foreach(byte com in compact)
            { assinatura.Add(com); }

            assinatura.Add(Verificador);

            foreach(byte com in replacedBytes)
            { assinatura.Add(com); }

            assinatura.Add(Fechamento);

            List<byte> newInput = new List<byte>( input);
            newInput.InsertRange(index, assinatura);
            return newInput.ToArray();
        }

        /// <summary>
        /// Compacta os bytes selecionados de forma inteligente
        /// </summary>
        /// <param name="bytesToCompact">Bytes para serem compactados</param>
        /// <param name="input">Stream principal</param>
        private void GenerateCompactByte(byte[] bytesToCompact, byte[] input)
        {
            List<byte[]> usedBytes = new List<byte[]>();
            foreach(byte currentByte in input)
            {
                byte[] b = new byte[1] { Transposicao[currentByte] };
                usedBytes.Add(b);
            }

            foreach(byte currentByte in UsedBytes)
            {
                byte[] b = new byte[1] { currentByte };
                usedBytes.Add(b);
            }

            List<byte[]> previamenteCompactos = Compactacao.Values.ToList();
            foreach(byte[] currentByte in previamenteCompactos)
            { usedBytes.Add(currentByte); }

            Random r = new Random(input[0]);
            List<byte[]> newCompactByte = new List<byte[]>();
            newCompactByte.Add(new byte[1] { (byte)r.Next() });
            int tentativas = 0;
            bool canCompact = false;

            while(!canCompact)
            {
                bool sameByte = false;
                canCompact = false;
                bool Compared = false;
                foreach (byte[] currentByte in usedBytes)
                {
                    if(newCompactByte[0].Length == currentByte.Length)
                    {
                        Compared = true;
                        for (int i = 0; i <newCompactByte[0].Length; i++)
                        {
                            int ncb = newCompactByte[0][i];
                            int cb = currentByte[i];

                            if(ncb == cb)
                            { sameByte = true; break; }
                        }
                    }

                    if (!sameByte) canCompact = true;
                    else { canCompact = false; }
                }

                if(!Compared)
                { canCompact = true; }

                if (!canCompact)
                {
                    r.NextBytes(newCompactByte[0]);
                    tentativas++;
                    if (tentativas > 300)
                    { newCompactByte[0] = new byte[newCompactByte[0].Length + 1]; r.NextBytes(newCompactByte[0]); tentativas = 0; }
                }
            }

            Compactacao.Add(bytesToCompact, newCompactByte[0]);
        }
    }
}