﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeSecurity
{
    [System.AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    sealed class SecurityLevelAttribute : Attribute
    {
        public EncryptionSecurityLevel SecurityLevel { get; private set; }
        public SecurityLevelAttribute(EncryptionSecurityLevel securityLevel)
        {
            SecurityLevel = securityLevel;
        }

        /// <summary>
        /// Nível de segurança personalizado: para sobrecarga de nível
        /// </summary>
        public int[] CustomizedLevels { get; set; }
    }

}
