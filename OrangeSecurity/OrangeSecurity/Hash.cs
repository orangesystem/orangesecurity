﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace OrangeSecurity
{
    internal partial class Hash : IOSTransformationInterface
    {
        byte[] IOSTransformationInterface.Assinatura { get { return Assinatura; } }
        byte IOSTransformationInterface.Verificador { get { return Verificador; } }
        EncryptionSecurityLevel IOSTransformationInterface.SecurityLevel { get { return SecurityLevel; } set { SecurityLevel = value; } }
        byte[] IOSTransformationInterface.HashCode { get { return HashCode; } }
        protected ExecuteMethod Handler { get { return _hand; }
            private set
            {
                if(Execute(value))
                { _hand = value; }
            }
        }

        private ExecuteMethod _hand;
        public byte[] DecryptData(byte[] Input, byte[] Key)
        {
            throw new NotImplementedException();
        }

        public byte[] EncryptData(byte[] Input, byte[] Key)
        {
            if (Input != _input)
            { throw new InvalidInputException("A entrada de dados deve ser a mesma usada na criação do Hash"); }

            /*Sequência:
            1 - Compacta os blocos : {A;.;V;.;F}
            2 - Transforma os bytes;
            3 - Assina os bytes de verificação(bytes de confusão);
            4 - Assina a integridade; {A;.;V;V;.;F}
            5 - Assina os bytes de verificação;
            6 - Assina os bytes de assinatura;
            */

            byte[] output;
            Handler = CompactBlock;
            output = Handler(Input);

            Handler = TransformBlock;
            output = Handler(output);

            Handler = SignIntegrityVerificator; //Verificar se há algum erro
            output = Handler(output);

            Handler = SignCompleteIntegrityVerificator;
            output = Handler(output);

            return output;
        }
        
        [SecurityLevel(EncryptionSecurityLevel.NoVerification)]
        private byte[] TransformBlock(byte[] input)
        {
            List<byte> transformation = new List<byte>();
            List<byte[]> compactedBytes = Compactacao.Values.ToList();
            int openBlocks = 0;
            bool warning = false, pause = false;

            for(int i= 0; i<input.Length; i++)
            {
                if(input[i] == Abertura) { openBlocks++; }
                if(input[i] == Fechamento) { openBlocks--; }

                if(openBlocks > 0)
                { pause = true; }

                warning = false;
                if (!pause)
                {
                    foreach (byte[] currentCompacted in compactedBytes)
                    {
                        if (currentCompacted[0] == input[i])
                        {
                            warning = true;
                            for (int index = 0; index < currentCompacted.Length; index++)
                            {
                                if (currentCompacted[index] != input[index + i])
                                { warning = false;break; }

                            }
                        }
                    }
                }

                if(!pause && !warning)
                { transformation.Add(Transposicao[input[i]]); }
                else
                { transformation.Add(input[i]); }

                if (openBlocks == 0) { pause = false; }
                if (openBlocks < 0) { throw new Exception(); }
            }

            return transformation.ToArray();
        }

        [SecurityLevel(EncryptionSecurityLevel.LowSecurity)]
        private byte[] CompactBlock(byte[] input)
        {
            List<byte[]> chaves = Compactacao.Keys.ToList();
            List<byte> outputBlock = input.ToList();

            foreach(byte[] currentChave in chaves)
            {
                outputBlock = ReplaceFromInput(currentChave, outputBlock);
                outputBlock = SignCompact(currentChave, outputBlock).ToList();
            }

            return outputBlock.ToArray();
        }

        [SecurityLevel(EncryptionSecurityLevel.LowSecurity)]
        private byte[] SignVerification(byte[] input)
        {
            bool assinar = false;
            List<byte> newInput = new List<byte>(input);
            if(SecurityLevel == EncryptionSecurityLevel.LowSecurity)
            {
                Random r = new Random();
                if(r.Next(10) > 4)
                { assinar = true; }
            }
            if(SecurityLevel > EncryptionSecurityLevel.LowSecurity)
            { assinar = true; }

            if(assinar)
            {  newInput.Insert(0, Verificador);  }

            return newInput.ToArray();
        }

        [SecurityLevel(EncryptionSecurityLevel.LowSecurity)]
        private byte[] SignBlock(byte[] input)
        {
            List<byte> newInput = new List<byte>(input);
            if(SecurityLevel == EncryptionSecurityLevel.LowSecurity)
            {
                //Se a segurança for baixa, assina apenas um byte de assinatura
                newInput.Insert(0, Assinatura[0]);
            }
            else if(SecurityLevel > EncryptionSecurityLevel.LowSecurity)
            { newInput.InsertRange(0, Assinatura); }

            return newInput.ToArray();
        }

        /// <summary>
        /// Assina valores de dados no documento
        /// </summary>
        /// <param name="input">input a inserir os dados</param>
        [SecurityLevel(EncryptionSecurityLevel.MediumSecurity)]
        private byte[] SignIntegrityVerificator(byte[] input)
        {
            int number = 0;
            if(input.Length >=30) { number = 5; }
            else if(input.Length >= 20) { number = 4; }
            else if(input.Length >= 15) { number = 3; }
            else if(input.Length >=10) { number = 2; }
            else if(input.Length >6) { number = 1; }

            byte[] output = input.ToArray();
            
            if(SecurityLevel == EncryptionSecurityLevel.MediumSecurity)
            {
                int anexes = number;
                int staticPosition = 0;
                List<bool> openPropertyTree = new List<bool>();
                bool countNow = false, newSection = true;
                List<byte> newInput = new List<byte>(input);
                List<byte> signature = new List<byte>();

                for(int currentByte = 0; currentByte < newInput.Count; currentByte++)
                {
                    if(anexes == 0) { break; }
                    if(newInput[currentByte] == Abertura) { openPropertyTree.Add(true); }
                    if(newInput[currentByte] == Fechamento) { openPropertyTree.RemoveAt(0); newSection = true; }

                    signature.Clear();
                    signature.Add(Abertura);
                    signature.Add(Verificador);
                    signature.Add(Verificador);
                    signature.Add(Fechamento);

                    if (openPropertyTree.Count == 0 && newSection)
                    {
                        Random r = new Random(newInput.Count);
                        countNow = (r.Next(10) > 5) ? true : false;

                        if(countNow)
                        {
                            int counter = 0;
                            staticPosition = currentByte;
                            int lastPreviewPosition = 0;
                            for (int positionNow = currentByte; positionNow < newInput.Count; positionNow++)
                            {
                                lastPreviewPosition = positionNow;
                                if(newInput[positionNow] == Abertura)
                                { currentByte = positionNow-1; break; }
                                if(newInput[positionNow] == Fechamento) { throw new Exception(); }

                                counter++;
                            }

                            byte[] counting = Encoding.Default.GetBytes(counter.ToString());
                            signature.InsertRange(1, counting);
                            newInput.InsertRange(staticPosition, signature);
                            try
                            {
                                currentByte = GoToNextProperty(lastPreviewPosition, newInput.ToArray()) - 1;
                            }
                            catch (Exception) { break; }
                            anexes--;
                            countNow = false;
                        }
                    }
                    newSection = false;
                }
                output = newInput.ToArray();
            }

            return output;
        }

        /// <summary>
        /// Assina completamente o documento 
        /// </summary>
        /// <param name="input">Input a ser complementada</param>
        [SecurityLevel(EncryptionSecurityLevel.RegularSecurity)]
        private byte[] SignCompleteIntegrityVerificator(byte[] input)
        {
            byte[] output = input.ToArray();

            int staticPosition = 0;
            List<bool> openPropertyTree = new List<bool>();
            bool newSection = true;
            List<byte> newInput = new List<byte>(input);
            List<byte> signature = new List<byte>();

            //for (int currentByte = 0; currentByte < newInput.Count; currentByte++)
            //{
            //    if (newInput[currentByte] == Abertura) { openPropertyTree.Add(true); }
            //    if (newInput[currentByte] == Fechamento) { openPropertyTree.RemoveAt(0); newSection = true; }

            //    signature.Clear();
            //    signature.Add(Abertura);
            //    signature.Add(Verificador);
            //    signature.Add(Verificador);
            //    signature.Add(Fechamento);

            //    if (openPropertyTree.Count == 0 && newSection)
            //    {
            //        float mather = 0f;
            //        int operation = 0;

            //        int counter = 0;
            //        staticPosition = currentByte;
            //        int lastPreviewPosition = 0;
            //        for (int positionNow = currentByte; positionNow < newInput.Count; positionNow++)
            //        {
            //            lastPreviewPosition = positionNow;
            //            if (newInput[positionNow] == Abertura)
            //            { currentByte = positionNow - 1; break; }
            //            if (newInput[positionNow] == Fechamento) { throw new Exception(); }

            //            int value;
            //            switch (operation)
            //            {
            //                case 0:
            //                    value = newInput[positionNow];
            //                    mather += value;
            //                    operation++;
            //                    break;

            //                case 1:
            //                    value = newInput[positionNow];
            //                    mather /= value;
            //                    operation++;
            //                    break;

            //                case 2:
            //                    value = newInput[positionNow];
            //                    mather *= value;
            //                    operation++;
            //                    break;

            //                case 3:
            //                    value = newInput[positionNow];
            //                    mather -= value;
            //                    operation = 0;
            //                    break;

            //                default:
            //                    operation = 0;
            //                    goto case 0;
            //            }

            //            counter++;
            //        }

            //        byte[] counting = Encoding.Default.GetBytes(counter.ToString());
            //        byte[] sum = Encoding.Default.GetBytes(mather.ToString());
            //        signature.InsertRange(3, sum);
            //        signature.InsertRange(1, counting);

            //        newInput.InsertRange(staticPosition, signature);
            //        try
            //        {
            //            currentByte = GoToNextProperty(lastPreviewPosition, newInput.ToArray()) - 1;
            //        }
            //        catch (Exception) { break; }
            //    }
            //    newSection = false;
            //}

            //output = newInput.ToArray();
            //return output;

            float mather = 0f;
            int operation = 0;
            int counter = 0;
            for (int currentByte = 0; currentByte < newInput.Count; currentByte++)
            {
                signature.Clear();
                signature.Add(Abertura);
                signature.Add(Verificador);
                signature.Add(Verificador);
                signature.Add(Fechamento);

                staticPosition = currentByte;
                if (!IsInsideAProperty(currentByte, newInput.ToArray()))
                {
                    int lastPreviewPosition = currentByte;
                    for (int positionNow = currentByte; positionNow < newInput.Count; positionNow++)
                    {
                        lastPreviewPosition = positionNow;
                        if (newInput[positionNow] == Abertura)
                        { currentByte = positionNow - 1; break; }
                        if (newInput[positionNow] == Fechamento) { throw new Exception(); }

                        int value;
                        switch (operation)
                        {
                            case 0:
                                value = newInput[positionNow];
                                mather += value;
                                operation++;
                                break;

                            case 1:
                                value = newInput[positionNow];
                                mather /= value;
                                operation++;
                                break;

                            case 2:
                                value = newInput[positionNow];
                                mather *= value;
                                operation++;
                                break;

                            case 3:
                                value = newInput[positionNow];
                                mather -= value;
                                operation = 0;
                                break;

                            default:
                                operation = 0;
                                goto case 0;
                        }
                        counter++;
                    }
                    byte[] counting = Encoding.Default.GetBytes(counter.ToString());
                    byte[] sum = Encoding.Default.GetBytes(mather.ToString());
                    signature.InsertRange(3, sum);
                    signature.InsertRange(1, counting);

                    newInput.InsertRange(staticPosition, signature);
                    try
                    { currentByte = GoToNextSection((staticPosition+signature.Count), newInput.ToArray()) - 1; }
                    catch (Exception) { break; }
                }

                else
                { currentByte = GoToNextSection(currentByte, newInput.ToArray()) - 1; }
            }

            return newInput.ToArray();
        }

        /// <summary>
        /// Insere byte verificador dentro da input em locais pseudoaleatorios com o intuito de dificultar a descoberta
        /// da chave
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [SecurityLevel(EncryptionSecurityLevel.MaxSecurity)]
        private byte[] SignVerificationBytes(byte[] input)
        { throw new NotImplementedException(); }

        /// <summary>
        /// Localiza a posição do proximo byte de abertura de propriedade
        /// </summary>
        /// <param name="currentIndexPosition">Posição atual na input</param>
        /// <param name="input">Input a ser analisada</param>
        /// <returns>Posição da próxima propriedade encontrada</returns>
        private int GoToNextProperty(int currentIndexPosition, byte[] input)
        {
            int position = currentIndexPosition;
            for(int i = position; i<input.Length; i++)
            {
                if(input[i] == Abertura)
                { position = i; break; }
            }

            if(position == currentIndexPosition)
            { throw new Exception("Não foi encontrado outra propriedade no documento"); }

            return position;
        }

        private int GoToNextSection(int currentIndexPosition, byte[] input)
        {
            int position = currentIndexPosition;
            List<bool> openPropertyTree = new List<bool>();

            //Verifica se há alguma propriedade aberta até o ponto a ser analisado
            for (int i = 0; i < position; i++)
            {
                if (input[i] == Abertura) { openPropertyTree.Add(true); }
                if (input[i] == Fechamento) { openPropertyTree.RemoveAt(0); }
            }

            //Analisa a partir do ponto desejado
            for(int i = position; i<input.Length; i++)
            {
                if(input[i] == Abertura) { openPropertyTree.Add(true); }
                if(input[i] == Fechamento)
                {
                    openPropertyTree.RemoveAt(0);
                    if (openPropertyTree.Count == 0)
                    {
                        if ((i + 1) <= (input.Length - 1))
                        { position = i + 1; break; }
                    }
                }
            }

            if(position == currentIndexPosition) { throw new Exception("Não existe seção a frente."); }

            return position;
        }

        /// <summary>
        /// Verifica se a atual posição está dentro de uma propriedade da input
        /// </summary>
        /// <param name="currentIndexPosition">Posição a ser analisada</param>
        /// <param name="input">Input a ser analisada</param>
        /// <returns>Verdadeiro se estiver dentro de uma propriedade</returns>
        private bool IsInsideAProperty(int currentIndexPosition, byte[] input)
        {
            int position = currentIndexPosition;
            List<bool> openPropertyTree = new List<bool>();

            //Verifica se há alguma propriedade aberta até o ponto a ser analisado
            for (int i = 0; i < position; i++)
            {
                if (input[i] == Abertura) { openPropertyTree.Add(true); }
                if (input[i] == Fechamento) { openPropertyTree.RemoveAt(0); }
            }

            return (openPropertyTree.Count > 0) ? true : false;
        }

        private bool Execute(Delegate e)
        {
            Attribute ab = e.Method.GetCustomAttribute(typeof(SecurityLevelAttribute));
            SecurityLevelAttribute secur = (SecurityLevelAttribute)ab;

            return (secur.SecurityLevel <= SecurityLevel) ? true : false;
        }

    }

    delegate byte[] ExecuteMethod(byte[] input);
}
