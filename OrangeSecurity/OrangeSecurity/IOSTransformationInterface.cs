﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeSecurity
{
    public interface IOSTransformationInterface
    {
        byte[] Assinatura { get; }
        byte Verificador { get; }
        byte[] HashCode { get; }
        EncryptionSecurityLevel SecurityLevel { get; set; }

        byte[] EncryptData(byte[] Input, byte[] Key);
        byte[] DecryptData(byte[] Input, byte[] Key);
    }

    /// <summary>
    /// Nível de segurança de criptografia e compactação: níveis maiores requerem uma capacidade maior de hardware.
    /// </summary>
    public enum EncryptionSecurityLevel
    {
        /// <summary>
        /// Nível maximo de segurança com verificações de dados e confirmação de integridade da criptografia. Requer uma quantidade maior de memória
        /// para armazenar os dados.
        /// </summary>
        MaxSecurity = 4,

        /// <summary>
        /// Nível de criptografia com uma menor quantidade de dados de segurança. Contém pelo menos uma chave de assinatura e uma de segurança
        /// </summary>
        RegularSecurity = 3,

        /// <summary>
        /// Nível de segurança intermediário: contém uma pelo menos uma assinatura, podendo conter ou não um verificador de integridade.
        /// </summary>
        MediumSecurity = 2,

        /// <summary>
        /// Nível de segurança criptografica baixa: sem verificação de integridade ou assinatura completa. Requer menos memória para armazenamento.
        /// </summary>
        LowSecurity = 1,

        /// <summary>
        /// Sem segurança criptográfica: não há assinaturas, verificação ou compactação. A quantidade de memória requerida é a mesma da entrada de dados
        /// </summary>
        NoVerification = 0
    }
}
